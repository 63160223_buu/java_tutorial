/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author focus
 */
public class SwingIntroduction {
    public static void main(String[] args) {
        JFrame frame = new JFrame(); // สร้าง JFrame 
        
        JButton b = new JButton("Click");
        b.setBounds(130, 100, 100, 40); //เซต ตำแหน่งแกน x แกน y ความกว้าง ความสูง
        
        frame.add(b); // เพิ่ม JButton b เข้าไปใน JFrame frame
        
        frame.setSize(400, 500); // กำหนดขนาดอง JFrame เป็น กว้าง 400 สูง 500
        frame.setLayout(null);// ตั้งเป็น null กล่าวคือใครก็ตามที่เข้ามาใช้ ต้องเป็นคนจัดรูปแบบเอง
        frame.setVisible(true);//ทำให้มองเห็นโปรแกรม

    }
}
