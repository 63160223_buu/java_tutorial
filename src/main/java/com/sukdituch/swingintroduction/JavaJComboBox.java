/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author focus
 */
public class JavaJComboBox {

    JFrame f;

    JavaJComboBox(){
        
        f = new JFrame("ComboBox Example");
        
        final JLabel label = new JLabel();// final คือไม่มีใครสามารถเปลี่ยนแปลงได้
        label.setHorizontalAlignment(JLabel.CENTER);//กำหนดตำแหน่งของ JLabel ให้อยู่ตรงกลาง
        label.setSize(400, 100);
        
        JButton b = new JButton("Show");
        b.setBounds(200, 100, 75, 20);
        
        String languages[] = {"C","C++","C#","Java","Python","PHP"}; // สร้าง Array String เพื่อเก็บชื่อภาษาต่างๆ
        
        final JComboBox cb = new JComboBox(languages); //เพิ่ม Array Languages เข้าไปใน JComboBox 
        cb.setBounds(50, 100, 90, 20);
        
        f.add(cb); f.add(b); f.add(label); // add ทุก object เข้าไปใน frame
        f.setLayout(null); //กำหมดให้ใครที่เข้ามาใช้โปรแกรม เป็นคนจัดองค์ประกอบต่างๆเอง
        f.setSize(350,350);//กำหนดขนาด frame กว้าง สูง
        f.setVisible(true);//ทำให้มองเห็น
        
        b.addActionListener(new ActionListener() { //เพิ่ม ActionListener ลงในปุ่ม โดย new ActionListener(สร้างขึ้นมาใหม่)
            @Override
            public void actionPerformed(ActionEvent e) {
        String data = "Programming Language Selected : "
                + cb.getItemAt(cb.getSelectedIndex()); //getSelectedIndex() จะคืนค่าตำแหน่งของข้อมูลที่ถูกเลือกโดยคืนค่าเป็นตัวแปรประเภท int
//getItemAt(index) คือ จะคืนค่าข้อมูล ณ ตำแหน่งที่กำหนด กลับมาเป็นตัวแปรประเภท Object โดย index คือตำแหน่งรายการข้อมูลที่ต้องการ
        label.setText(data); //กำหนดให้ label แสดงผลเป็นข้อมูลของตัวแปร data
            }
        });     
    }
    
    public static void main(String[] args) {
        new JavaJComboBox();
    }
    
}
