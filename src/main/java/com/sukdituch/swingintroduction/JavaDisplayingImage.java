/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.JFrame;

/**
 *
 * @author focus
 */
//Canvas จัดอยู่ในกลุ่มของ AWT Component ใช้สำหรับการสร้างวาดภาพ และออกแบบกราฟฟิกบน GUI เช่น 
//การวาดเส้นกราฟ วาดลวดลาย ต่าง ๆ
public class JavaDisplayingImage extends Canvas {

    public void paint(Graphics g) {

        Toolkit t = Toolkit.getDefaultToolkit();//เครื่องมือต่างๆใน java
        Image i = t.getImage("D:\\chuunibyou.gif");//add รูปภาพ
        g.drawImage(i, 120, 100, this);//(image, int x, int y, Image Observer)

    }

    public static void main(String[] args) {

        JavaDisplayingImage m = new JavaDisplayingImage();
        JFrame f = new JFrame();
        
        f.add(m);
        f.setSize(400, 400);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

}
