/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSeparator;

/**
 *
 * @author focus
 */
public class JavaJSeparatorV_2 {

    public static void main(String args[]) {
        
        JFrame f = new JFrame("Separator Example");
        f.setLayout(new GridLayout(0, 1)); //GridLayout เป็นรูปแบบของการจัดวาง layout โดยจะจัดวาง component 
        //แบบตาราง คือมีการกำหนดจำนวนแถว และคอลัมน์ และจะเริ่มวาง component จากซ้ายไปขวา
        //เมื่อหมดบรรทัดจะขึ้นบรรทัดใหม่ให้ ( ถึงแม้จะเกินแถวที่กำหนดก็ตาม )
        
        JLabel l1 = new JLabel("Above Separator");
        f.add(l1);
        
        JSeparator sep = new JSeparator();
        f.add(sep);
        
        JLabel l2 = new JLabel("Below Separator");
        f.add(l2);
        f.setSize(400, 100);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
    }
}

