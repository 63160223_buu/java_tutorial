/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;

/**
 *
 * @author focus
 */
public class JavaJSlider extends JFrame{
    
    public JavaJSlider(){
        
        JSlider slider = new JSlider(JSlider.HORIZONTAL, 0, 50, 25);//HORIZONTAL แปลว่า แนวนอน
        
        slider.setMinorTickSpacing(2);//กำหนดว่าแต่ละ Tick บน Slider ห่างกันกี่หน่วยข้อมูล
        slider.setMajorTickSpacing(10);//กำหนดว่าแต่ละ ป้ายบอกข้อมูล บน Slider ห่างกันกี่หน่วยข้อมูล
        slider.setPaintTicks(true);//กำหนดว่าจะให้แสดง Tick เพื่อแสดงถึงแต่ละช่วงของข้อมูล บน Slider
        slider.setPaintLabels(true);//กำหนดว่าจะให้แสดง ป้ายบอกข้อมูล บน Slider
        
        JPanel panel = new JPanel();
        panel.add(slider); // add slider ลงไปใน panel
        add(panel); //add panel ลงใน JFrame
        
    }
    
    public static void main(String[] args) {
        
        JavaJSlider frame = new JavaJSlider();
        frame.pack();//การจัดหมวดหมู่หรือแบ่งแยกกลุ่มของคลาสให้เป็นกลุ่มๆ
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
    }
    
}
