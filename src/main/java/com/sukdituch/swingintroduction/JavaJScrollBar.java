/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollBar;

/**
 *
 * @author focus
 */
public class JavaJScrollBar {

    public JavaJScrollBar() {

        JFrame f = new JFrame("Scrollbar");
        final JLabel label = new JLabel();
        label.setHorizontalAlignment(JLabel.CENTER);//กำหนดตำแหน่งของ JLabel
        label.setSize(400, 100);

        final JScrollBar sb = new JScrollBar(); //สร้าง Object JScrollBar
        sb.setBounds(370, 0, 10, 380);//กำหนดตำแหน่ง x, y, กว้าง, สูง

        f.add(sb);
        f.add(label);
        f.setSize(400, 400);
        f.setLayout(null);//กำหนดให้ใครก็ตามที่เข้ามาใช้โปรแกรมเป็นคนจัดองค์ประกอบเอง
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        //AdjustmentListener()เป็น Interface ในการ ดักฟังเหตุการณ์ ของการทำงานของ mouse wheel(ลูกเลื่อนเมาส์)
        sb.addAdjustmentListener(new AdjustmentListener(){
            @Override
            public void adjustmentValueChanged(AdjustmentEvent e) {
                label.setText("Vertical Scrollbar values is : "+ sb.getValue());
            }
        
        });
    }
    
    public static void main(String[] args) {
        new JavaJScrollBar();
    }
    
}
