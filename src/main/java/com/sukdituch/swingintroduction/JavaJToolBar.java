/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import java.awt.BorderLayout;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;

/**
 *
 * @author focus
 */
public class JavaJToolBar {
    public static void main(String[] args) {
        
        JFrame myframe = new JFrame("JToolBar Example");
        myframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JToolBar toolbar = new JToolBar();// สร้าง JToolBar
        toolbar.setRollover(true);//Rollover การกลิ้ง
        
        JButton button = new JButton("File");
        toolbar.add(button);
        toolbar.addSeparator();//Separator คือ การทำเป็นกรอบเพื่อแบ่งแยกแต่ละอัน
        toolbar.add(new JButton("Edit"));
        toolbar.add(new JComboBox(new String[]{"Opt-1","Opt-2","Opt-3","Opt-4"}));
        
        Container contentpane = myframe.getContentPane();//Container ที่จัดเก็บข้อมูลต่างๆ
        //BorderLayout เป็นรูปแบบการจัดส่ง Layout ที่กำหนดตำแหน่งแบบแน่นอนคือจะประกอบด้วย NORTH (ด้านบน), 
        //SOUTH (ด้านล่าง), EAST (ขวา), WEST (ซ้าย), and CENTER (ตรงกลาง) ซึ่งจะประกอบด้วย 5 ตำแหน่ง
        contentpane.add(toolbar, BorderLayout.NORTH);
        
        JTextArea textArea = new JTextArea();//กล่องใส่ข้อมูล
        JScrollPane mypane = new JScrollPane(textArea);//ตัวเลื่อน
        contentpane.add(mypane, BorderLayout.EAST);//BorderLayout คือการสร้างที่อยู่แบบแน่นอนเฉพาะเจาะจง
        
        myframe.setSize(450, 250);
        myframe.setVisible(true);
    }
            
    
}
