/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author focus
 */
public class JavaJSpinner {

    public static void main(String[] args) {
        JFrame f = new JFrame("Spinner Example");

        //JLabel คือข้อความที่ไม่สามารถทำอะไรกับมันได้
        final JLabel label = new JLabel(); //final คือ ไม่สามารถเปลี่ยนแปลงค่าได้
        label.setHorizontalAlignment(JLabel.CENTER);//Horizontal แนวนอน
        label.setSize(250, 100);

        // สร้างตัวข้อมูลเพื่อนำไปใส่ใน JSpinner
        SpinnerModel value = new SpinnerNumberModel(5,//5 คือ ค่าเริ่มต้น
                0, // 0 คือค่าน้อยสุดในโปรแกรม
                10, // 10 คือค่ามากสุดในโปรแกรม
                1);//1 คือ เลื่อนได้ทีละ1 สเต็ป

        JSpinner spinner = new JSpinner(value);// นำค่าใน value มาใส่ใน JSpinner
                spinner.setBounds(100, 100, 50, 30);//กำหนดตำแหน่ง x, y, กว้าง, สูง
                
                f.add(spinner);
                f.add(label);
                f.setSize(300, 300);
                f.setVisible(true);
                f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                
                //เป็น Interface ในการ ดักฟังเหตุการณ์ ของการทำงานของ State เช่น JTabbedPane, JMenu,
                // JMenuItem, JProgressBar, JSliderโดยมี abstract method คือ stateChanged คือจะทำงานเมื่อ component เปลี่ยนแปลง state
                spinner.addChangeListener(new ChangeListener(){
            @Override
            public void stateChanged(ChangeEvent e) {
                label.setText("Value : "+((JSpinner)e.getSource()).getValue());//getSource ใช้คืนค่า ชื่อของ component ที่ทำให้เกิดเหตุการณ์ขึ้น
                                                                            //getValue() ใช้คืนค่าค่าข้อมูลของ table ในแถวและคอลัมน์ที่กำหนด  
            }          
                });          
    }
}
