/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.AbstractButton;
import javax.swing.Icon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 *
 * @author focus
 */
public class JavaJCheckBoxMenuItem {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Jmenu Example");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JMenuBar menuBar = new JMenuBar();
        //File Menu, F - Mnemonic     
        JMenu fileMenu = new JMenu("File");
        fileMenu.setMnemonic(KeyEvent.VK_F); //.setMnemonic คือ กำหนด Short Key ให้กับ filemenu โดย key ลัดคือ VK_F
        menuBar.add(fileMenu);

        // File->New, N - Mnemonic
        JMenuItem menuItem1 = new JMenuItem("Open", KeyEvent.VK_N);//.setMnemonic คือ กำหนด Short Key ให้กับ menuItem1 โดย key ลัดคือ VK_N
        fileMenu.add(menuItem1);

        JCheckBoxMenuItem caseMenuItem = new JCheckBoxMenuItem("Option_1");
        caseMenuItem.setMnemonic(KeyEvent.VK_C);//.setMnemonic คือ กำหนด Short Key ให้กับ caseMenuItem โดย key ลัดคือ VK_C
        fileMenu.add(caseMenuItem);

        ActionListener aListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                AbstractButton aButton = (AbstractButton) event.getSource(); //ให้ aButton เป็น Abstract โดยส่งข้อมูลเมื่อมีการกระทำกับมัน

                boolean selected = aButton.getModel().isSelected(); //.getModel ใช้คืนค่า object TableModel จาก object JTable
                String newLabel;
                Icon newIcon; //สร้าง Icon
                
                if(selected){ // ถ้าMenu ถูกเลือก
                    newLabel = "Value-1";
                }else{
                    newLabel = "Value-2";
                }
                aButton.setText(newLabel); //กำหนดให้ aButton ส่งข้อความไปตามที่ผู้ใช้เลือก
            }

        };

        caseMenuItem.addActionListener(aListener);// เพิ่ม ActionListener(aListener)ลงใน caseMenuItem
        frame.setJMenuBar(menuBar);//กำหนดให้ MenuBar เป็นไปตามที่เราเซตค่าไว้ใน menuBar
        frame.setSize(350, 250);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
    }
}
