/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLayeredPane;

/**
 *
 * @author focus
 */
public class JavaJLayeredPane extends JFrame {

    public JavaJLayeredPane() {

        super("LayeredPane Example"); //สืบทอดจาก JFrame
        setSize(200, 200);
        JLayeredPane pane = getLayeredPane(); // ทำงานซ้อนทำ layer

        JButton top = new JButton();
        top.setBackground(Color.white);
        top.setBounds(20, 20, 50, 50);

        JButton middle = new JButton();
        middle.setBackground(Color.red);
        middle.setBounds(40, 40, 50, 50);

        JButton bottom = new JButton();
        bottom.setBackground(Color.cyan);
        bottom.setBounds(60, 60, 50, 50);

        //add ปุ่ม ใน pane
        pane.add(bottom, new Integer(1)); //ชั้นที่1 ในสุด
        pane.add(middle, new Integer(2));//ชั้นที่2
        pane.add(top, new Integer(3));//ชั้นนอกสุด
        
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        JavaJLayeredPane panel = new JavaJLayeredPane();
        panel.setVisible(true);
    }
    
}
