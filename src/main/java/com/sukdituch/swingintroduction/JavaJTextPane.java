/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

/**
 *
 * @author focus
 */
public class JavaJTextPane {

    public static void main(String args[]) throws BadLocationException {

        JFrame frame = new JFrame("Misssine NotePad");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Container cp = frame.getContentPane();
        JTextPane pane = new JTextPane();

        SimpleAttributeSet attributeSet = new SimpleAttributeSet(); //การใช้งาน MutableAttributeSet อย่างตรงไปตรงมาโดยใช้ตารางแฮช

        StyleConstants.setBold(attributeSet, true); //bold คือ ตัวเข้ม

        //เซต Attr ก่อนเพิ่มลงใน text
        pane.setCharacterAttributes(attributeSet, true);//เซตตัวอักขระ
        pane.setText("Welcome"); //กำหนด TEXT 

        attributeSet = new SimpleAttributeSet();
        StyleConstants.setItalic(attributeSet, true);//setItalic คือ ทำให้ตัวอักขระเอียง
        StyleConstants.setForeground(attributeSet, Color.pink);
        StyleConstants.setBackground(attributeSet, Color.gray);

        Document doc = pane.getStyledDocument();
        doc.insertString(doc.getLength(), "To Misssine", attributeSet);

        attributeSet = new SimpleAttributeSet();
        doc.insertString(doc.getLength(), "NotePad", attributeSet);

        JScrollPane scrollPane = new JScrollPane(pane);
        cp.add(scrollPane, BorderLayout.CENTER); //BorderLayout คือ กำหนดที่ตั้ง

        frame.setSize(400, 300);
        frame.setVisible(true);
    }
}
