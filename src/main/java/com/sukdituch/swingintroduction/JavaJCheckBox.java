/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 *
 * @author focus
 */
public class JavaJCheckBox extends JFrame implements ActionListener {//สืบทอด JFrame และ Implements ActionListener

    JLabel l;
    JCheckBox cb1, cb2, cb3;
    JButton b;

    JavaJCheckBox() {
        l = new JLabel("Food Ordering System");
        l.setBounds(50, 50, 300, 20);

        cb1 = new JCheckBox("Pizza @ 100");
        cb1.setBounds(100, 100, 150, 20);
        cb2 = new JCheckBox("Burger @ 30");
        cb2.setBounds(100, 150, 150, 20);
        cb3 = new JCheckBox("Tea @ 10");
        cb3.setBounds(100, 200, 150, 20);

        b = new JButton("Order");
        b.setBounds(100, 250, 80, 30);
        b.setBackground(Color.pink);
        b.addActionListener(this);

        add(l);
        add(cb1);
        add(cb2);
        add(cb3);
        add(b);
        setSize(400, 400);
        setLayout(null);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        float amount = 0;
        String msg = "";
        if (cb1.isSelected()) {
            amount += 100; // amount = amount + 100
            msg = "Pizza: 100\n"; // \n คือขึ้นบรรทัดใหม่
        }
        if (cb2.isSelected()) {
            amount += 30;// amount = amount +30
            msg += "Burger: 30\n";// \n คือขึ้นบรรทัดใหม่
        }
        if (cb3.isSelected()) {
            amount += 10;// amount = amount + 10
            msg += "Tea: 10\n";// \n คือขึ้นบรรทัดใหม่
        }
        msg += "-----------------\n";// \n คือขึ้นบรรทัดใหม่
        JOptionPane.showMessageDialog(this, msg + "Total: " + amount);// ทำให้แสดงหน้าต่างขึ้นมาอีกอัน
    }

    public static void main(String[] args) {
        new JavaJCheckBox();
    }

}
