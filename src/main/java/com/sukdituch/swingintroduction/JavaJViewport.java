/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.border.LineBorder;

/**
 *
 * @author focus
 */
public class JavaJViewport {

    public static void main(String[] args) {

        JFrame frame = new JFrame("Tabbed Pane Sample");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JLabel label = new JLabel("Label");
        label.setPreferredSize(new Dimension(1000, 1000));//ใช้กำหนด preferred size ให้กับ jcomponent ในรูปแบบของ dimension.
        label.setBackground(Color.white);
        label.setOpaque(true);
        JScrollPane jsp = new JScrollPane(label);

        JButton b1 = new JButton();
        jsp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);//Horizontalใช้คืนค่า ตัวเลข ที่เป็นค่าคงที่ ที่แทนความหมายว่า จะให้ scrollbar เป็นลักษณะ แนวนอน
        jsp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);//Vertical คือ แนวตั้ง
        jsp.setViewportBorder(new LineBorder(Color.PINK));
        jsp.getViewport().add(b1, null);//(component, int)

        frame.add(jsp, BorderLayout.CENTER);
        frame.setSize(400, 150);
        frame.setVisible(true);

    }
}
