/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import javax.swing.JFrame;
import javax.swing.JProgressBar;

/**
 *
 * @author focus
 */
public class JavaJProgressBar extends JFrame { // สืบทอดมาจาก JFrame

    JProgressBar jb;

    int i = 0, num = 0;

    JavaJProgressBar() {

        jb = new JProgressBar(0, 2000); //(int min, int max)
        jb.setBounds(40, 40, 160, 30);
        jb.setValue(0);
        jb.setStringPainted(true);//แสดงข้อความเพื่อบอก Percent ความคืบหน้าของ ProgressBar
        
        add(jb); // add (jb) ลงใน JFrame
        
        setSize(250, 150);
        setLayout(null);//กำหนดให้ใครก็ตามที่เข้ามาใช้งานโปรแกรม เป็นคนจัดองค์ประกอบต่างๆเอง
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    
    }
    
    //Iterator เป็น tag interface สำหรับติดที่คลาส เพื่อระบุว่า instances ของคลาสนั้น ถูกทำ iteration ได้ แต่ Iterator มี methods ดังนี้
    public void iterate(){ 
        
        while (i <= 2000){ //while คือ ถ้าเงื่อนไขเป็นจริงจะทำงานภายใน Loop ใน {}
        jb.setValue(i);
        i = i+20;

        try{Thread.sleep(150);}catch(Exception e){} //Thread คือ วงจรชีวิต
        }
    }
    
    public static void main(String[] args) {
        
        JavaJProgressBar pgb = new JavaJProgressBar();
        pgb.setVisible(true);
        pgb.iterate();
        
    }
    
}
