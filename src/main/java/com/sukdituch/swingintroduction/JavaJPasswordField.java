/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author focus
 */
public class JavaJPasswordField {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Password Field"); //สร้าง frame
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//กำหนดให้เมื่อปิดโปรแกรมจะหยุดรันใน Java
        
        final JLabel label = new JLabel(); // final คือ ไม่สามารถเปลี่ยนแปลงได้
        label.setBounds(20, 150, 200, 50);// กำหนดตำแหน่ง ขนาด
        
        final JPasswordField value = new JPasswordField(); //สร้างobject ตัวแปรเพื่อมาเก็บ password
        value.setBounds(100, 75, 100, 30); //กำหนดตำแหน่ง ขนาด
        
        JLabel l1 = new JLabel("Username: "); //JLabel เป็นข้อความที่ไม่สามารถทำอะไรกับมันได้
        l1.setBounds(20, 20, 80, 30);//กำหนดตำแหน่ง ขนาด
        JLabel l2 = new JLabel("Password: ");
        l2.setBounds(20, 75, 80, 30);
        
        JButton b = new JButton("Login"); // JButton คือ ปุ่ม
        b.setBounds(100, 120, 80, 30);
        
        final JTextField text = new JTextField(); // JTextField คือ กล่องข้อความ
        text.setBounds(100, 20, 100, 30);
        
        frame.add(value); frame.add(l1); frame.add(label); frame.add(l2); frame.add(b); frame.add(text); //เพื่มลงในframe
        frame.setSize(300, 300); //เซตขนาดของ frame
        frame.setLayout(null); // กำหนดให้ผู้ที่เข้ามาแก้ไข เป็นคนแก้ไของค์ประกอบต่างๆเอง
        frame.setVisible(true);//กำหนดให้มองเห็น frame ที่เราสร้าง
        
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String data = "Username " + text.getText(); //กำหนดให้ data มีข้อความเหมือน text
                data += ", Password: " + new String(value.getPassword());   // += คือ data = data +  password
                label.setText(data);
            }
        });
        
                
    }
    
}
