/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author focus
 */
public class JavaDigitalWatch implements Runnable{
    
    JFrame f;
    Thread t = null;//Thread คล้ายๆกับวงจรชีวิต
    
    int hours = 0, minutes = 0, seconds = 0;  
    String timeString = "";
    JButton b;
    
    JavaDigitalWatch(){
        f = new JFrame();
        
        t = new Thread(this);
        t.start();
        
        b = new JButton();
        b.setBounds(100, 100, 100, 50);
        
        f.add(b);
        f.setSize(300, 400);
        f.setLayout(null);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    @Override
    public void run() {
        try{
            
            Calendar cal = Calendar.getInstance();//นาฬิกา
            hours = cal.get(Calendar.HOUR_OF_DAY);//ชม
            if(hours > 12) hours -= 12;
            minutes = cal.get(Calendar.MINUTE);
            seconds = cal.get( Calendar.SECOND );
            
            //SimpleDateFormat เป็นการจัด Format ของเวลาให้อยู่ในรูปแบบที่เราต้องการ โดยเราสามารถกำหนดได้ว่า
            //จะให้ วัน เดือน ปี ชั่วโมง นาที วินาที อยู่ในตำแหน่งไหนก็ได้ 
            SimpleDateFormat formatter = new SimpleDateFormat("hh:mm:ss");
            Date date = cal.getTime();
            timeString = formatter.format(date);
            
            printTime();
            
            t.sleep(1000); //ช่วงเวลาที่กำหนดเป็นมิลลิวินาที
            
            
        }catch(Exception e){
            
        }
    }

    private void printTime() {
        b.setText(timeString);
    }
    
    public static void main(String[] args) {
        new JavaDigitalWatch();
    }
    
}
