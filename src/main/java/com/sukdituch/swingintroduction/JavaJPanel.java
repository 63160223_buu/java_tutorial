/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author focus
 */
public class JavaJPanel {

    JavaJPanel() {

        JFrame f = new JFrame("Panel Example");

        JPanel panel = new JPanel();//JPanel คือ การจัดกลุ่มของ Component Controls ให้อยู่ในกลุ่มเดียวกัน
        panel.setBounds(40, 80, 200, 200);
        panel.setBackground(Color.pink);

        JButton b1 = new JButton("Button 1");
        b1.setBounds(50, 100, 80, 30);
        b1.setBackground(Color.white);
        JButton b2 = new JButton("Button 2");
        b2.setBounds(100, 100, 80, 30);
        b2.setBackground(Color.black);
        
        panel.add(b1);
        panel.add(b2);
        
        f.add(panel);//เพิ่ม panel ลงใน frame
        f.setSize(400, 400);
        f.setLayout(null);//กำหนดให้ใครที่เข้ามาจัดการโปรแกรม เป็นคนจัดการองค์ประกอบต่างๆเอง
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
    }

    public static void main(String[] args) {
        new JavaJPanel();
    }
    
}
