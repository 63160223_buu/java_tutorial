/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import javax.swing.JFrame;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 *
 * @author focus
 */
public class JavaJTree {
    
    JFrame f;
    
    JavaJTree() {
        
        f = new JFrame();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //DefaultMutableTreeNode คือ สร้างรายการข้อมูล
        DefaultMutableTreeNode style = new DefaultMutableTreeNode("Style");
        DefaultMutableTreeNode color = new DefaultMutableTreeNode("Color");
        DefaultMutableTreeNode font = new DefaultMutableTreeNode("Font");
        
        style.add(color); // add color ลงใน style
        style.add(font);
        
        DefaultMutableTreeNode red = new DefaultMutableTreeNode("Red");
        DefaultMutableTreeNode blue = new DefaultMutableTreeNode("Blue");
        DefaultMutableTreeNode black = new DefaultMutableTreeNode("Black");
        DefaultMutableTreeNode pink = new DefaultMutableTreeNode("Pink");
        
        color.add(red);
        color.add(blue);
        color.add(black);
        color.add(pink);
        
        JTree jt = new JTree(style); //สร้าง JTree ขึ้นมา โดยใช้ข้อมูล style
        
        f.add(jt);
        f.setSize(200, 200);
        f.setVisible(true);
        
    }
    
    public static void main(String[] args) {
        new JavaJTree();
    }
    
}
