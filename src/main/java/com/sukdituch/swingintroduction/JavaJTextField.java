/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

/**
 *
 * @author focus
 */
public class JavaJTextField implements ActionListener{

    JTextField tf1, tf2, tf3; // สร้างตัวแปร JTextField ไว้นอก Method
    JButton b1, b2; // สร้างตัวแปร JButton ไว้นอก Method

    public JavaJTextField() { //
        JFrame frame = new JFrame(); // สร้าง JFrame
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //กำหนดให้เมื่อปิดโปรแกรม จำทำให้Javaหยุดrunโดยอัตโนมัติ
        tf1 = new JTextField(); // สร้าง JTextField ที่มีชื่อว่า tf1
        tf1.setBounds(50, 50, 150, 20); // กำหนดตำแหน่ง และ ขนาดของ tf1
        tf2 = new JTextField(); // สร้าง JTextField ที่มีชื่อว่า tf2
        tf2.setBounds(50, 100, 150, 20); // กำหนดตำแหน่ง และ ขนาดของ tf2
        tf3 = new JTextField(); // สร้าง JTextField ที่มีชื่อว่า tf3
        tf3.setBounds(50, 150, 150, 20); // กำหนดตำแหน่ง และ ขนาดของ tf3
        tf3.setEditable(false); //กำหนดให้การแก้ไขข้อความใน tf3 เป็น false คือ ไม่สามารถแก้ไขได้
        
        b1 = new JButton("+"); //สร้างปุ่ม +
        b1.setBounds(50,200,50,50); // กำหนดตำแหน่ง และ ขนาด b1
        b2 = new JButton("-"); //สร้างปุ่ม -
        b2.setBounds(120,200,50,50); // กำหนดตำแหน่ง และ ขนาด b1
        b1.addActionListener(this);
        b2.addActionListener(this);
        
        frame.add(tf1); frame.add(tf2); frame.add(tf3); frame.add(b1); frame.add(b2); // เพิ่ม tf และ b เข้าไปใน frame
        frame.setSize(300, 300); // กำหนดขนาด frame กว้าง 300 สูง 300
        frame.setLayout(null); // กำหนดให้ใครที่เข้ามาเปลี่ยน เป็นคนจัดรูปแบบเอง
        frame.setVisible(true); //กำหนดให้มองเห็น frame
        
    }
    
    
    @Override
    public void actionPerformed(ActionEvent e) {
        String s1 = tf1.getText(); // กำหนดให้ s1 มีค่า = tf1
        String s2 = tf2.getText();//  กำหนดให้ s2 มีค่า = tf2
        
        int a = Integer.parseInt(s1); // กำหนดให้ a เก็บค่า tf1 ที่แปลงเป็นจำนวนจริงไว้
        int b = Integer.parseInt(s2); // กำหนดให้ b เก็บค่า tf2 ที่แปลงเป็นจำนวนจริงไว้ 
        int c = 0; //กำหนดให้ c = 0
        
        //e.getsource() คือ ใช้คืนค่า ชื่อของ component(ส่วนประกอบ) ที่ทำให้เกิดเหตุการณ์ขึ้น
        if(e.getSource() == b1){ //ถ้าคืนค่าแล้วมีค่าเท่ากับ b1
            c = a+b;
         }else if(e.getSource() == b2){ //ถ้าคืนค่าแล้วมีค่าเท่ากับ b2
             c=a-b;
         }
        
        String result = String.valueOf(c); //กำหนดให้ตัวแปร c อยู่ในรูปแบบ String
        tf3.setText(result);//กำหนดให้ tf3 มีค่า เท่ากับ result
            
    }
    
     public static void main(String[] args) {
         new JavaJTextField(); // สร้าง Object
    }
}
