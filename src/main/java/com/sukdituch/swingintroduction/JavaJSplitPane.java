/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import java.awt.FlowLayout;
import java.awt.Panel;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JSplitPane;

/**
 *
 * @author focus
 */
public class JavaJSplitPane {
    
    private static void createAndShow(){// {}Anonymous Class
         // Create and set up the window.  
        final JFrame frame = new JFrame("JSplitPane Example");   //final คือไม่สามารถเปลี่ยนแปลงค่าได้
        
        //Display the window.  
        frame.setSize(300, 300);  
        frame.setVisible(true);  
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
        
         // set flow layout for the frame  
        frame.getContentPane().setLayout(new FlowLayout());  //FlowLayout คือ รูปแบบของการจัดวาง layout 
        //โดยจะเรียง component จากซ้ายไปขวา และถ้าหากหมดบรรทัดก็จะขึ้นต้นบรรทัดใหม่ เหมือนการเขียนหนังสือ
        
        String [] option1 = { "A","B","C","D","E" };  
        JComboBox box1 = new JComboBox(option1);
        String[] option2 = {"1","2","3","4","5"};  
        JComboBox box2 = new JComboBox(option2);  
        
        Panel panel1 = new Panel(); //Panel คือ การจัดกลุ่ม
        panel1.add(box1);
        Panel panel2 = new Panel();  
        panel2.add(box2);
        
        //JSplitPane กลุ่มของ Container ใช้สำหรับสร้างขอบควบคุ่มการแสดงผลระหว่าง Component Controls จำนวน 2 ตัว โดยสามารถย่อและขยาย เลื่อน ซ้าย-ขวา ระหว่าง 2 Controls ได้
        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, panel1, panel2);  
        
        frame.getContentPane().add(splitPane);
        
    }
    
    public static void main(String[] args) {
        
        //javax.swing.SwingUtilities.invokeLater(new Runnable() คือ การอัพเดทข้อมูล โดยนำข้อมูลที่อัพเดทไปต่อคิวไว้ เมื่อถึงคิวก็จะรัน อัพเดทให้เอง
        javax.swing.SwingUtilities.invokeLater(new Runnable() { 
            @Override
            public void run() {
               createAndShow();// เมื่อสร้างเสร็จแล้วให้แสดงข้อมูล
            }
        });//
        
    }
    
}
