/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import static java.awt.SystemColor.text;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;

/**
 *
 * @author focus
 */
public class JavaJTextArea implements ActionListener{

    JLabel l1, l2; // สร้างตัวแปรนอก Method
    JTextArea area;
    JButton b;
            
    public JavaJTextArea(){
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // กำหนดให้ Java หยุด Run เมื่อปิดโปรแกรม
        l1 = new JLabel("Words: 0"); // สร้าง JLabel 
        l1.setBounds(50, 25, 100, 30); //กำหนดตำแน่ง และ ขนาด
        l2 = new JLabel("Characters: 0"); // สร้าง JLabel 
        l2.setBounds(160, 25, 100, 30); //กำหนดตำแน่ง และ ขนาด
       
        area = new JTextArea();
        area.setBounds(20, 75, 250, 200);
        
        b = new JButton("Count Words"); // สร้างปุ่ม
        b.setBounds(100, 300, 120, 30); //กำหนดตำแน่ง และ ขนาด
        b.addActionListener(this);
        
        frame.add(l1); frame.add(l2); frame.add(area); frame.add(b);
        frame.setSize(450, 450);//กำหนดขนาด
        frame.setLayout(null); //กำหนดให้ผู้เข้าเข้ามาให้งานเป็นคนจัดการจัดองค์ประกอบต่างๆเอง
        frame.setVisible(true);
        
    }
            
    @Override
    public void actionPerformed(ActionEvent e) {
        String text = area.getText(); //กำหนดให้ text มีค่าเท่ากับ area
        String words[]=text.split("\\s");// .split("\\") คือแยก String ออกจากกันเช่น Hello Everyone ก็จะเป็น Hello กับ Everyone
        l1.setText("Words: "+words.length); //นับความยาวของคำ
        l2.setText("Characters: "+text.length());//นับจำนวนตัวอักษร
        
    }
    
    public static void main(String[] args) {
        new JavaJTextArea();
    }
    
}
