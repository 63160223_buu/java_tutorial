/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JTextArea;

/**
 *
 * @author focus
 */
public class JavaJColorChooser extends JFrame implements ActionListener{

    JFrame f;
    JButton b;
    JTextArea txtarea;//เป็นกรอบข้อความ
    
    JavaJColorChooser(){
        
        f = new JFrame("Color Chooser Test");
        
        b = new JButton("Pad Color");
        b.setBounds(200, 250, 100, 30);
        
        txtarea = new JTextArea();
        txtarea.setBounds(10, 10, 300, 200);//กำหนดตำแหน่ง x,y กว้าง,สูง
        
        b.addActionListener(this);
        f.add(b);
        f.add(txtarea);
        f.setLayout(null);
        f.setSize(400, 400);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        //JColorChooser คือ ผังสีที่มีสีมากมาย
       Color c = JColorChooser.showDialog(this, "Choose", Color.CYAN);//showDialog คือ show สิ่งภายในวงเล็ป
       
       txtarea.setBackground(c);// ใส่สีใน txtarea ตามสีที่เลือกในโปรแกรม
    }
    
    public static void main(String[] args) {
        new JavaJColorChooser();
    }
    
}
