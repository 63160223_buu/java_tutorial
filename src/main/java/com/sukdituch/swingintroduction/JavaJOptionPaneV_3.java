/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author focus
 */
public class JavaJOptionPaneV_3 {
    JFrame f;
    
    JavaJOptionPaneV_3(){
        f = new JFrame();
        
        // JOptionPane.showInputDialog คือ สามารถ Input ข้อความได้
        String name = JOptionPane.showInputDialog(f,"Enter Name");
        
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    public static void main(String[] args) {
        new JavaJOptionPaneV_3();
    }
}
