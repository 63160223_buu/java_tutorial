/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import javax.swing.JEditorPane;
import javax.swing.JFrame;

/**
 *
 * @author focus
 */
public class JavaJEditorPane {//คือสร้าง frame คล้ายๆ notepad ซึ่งสามารถกำหนดข้อความลงไปได้และสามารถแก้ไขข้อความได้

    JFrame myFrame = null; //กำหนดให้ JFrame ไม่มีค่าอะไร

    public static void main(String[] a) {
        (new JavaJEditorPane()).test();// test คือเราสร้าง Method ขึ้นมา
        //JPane คือการจัดกลุ่มกัน
    }

    private void test() {
        myFrame = new JFrame("JEditorPane Test");
        myFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        myFrame.setSize(400, 200);

        JEditorPane myPane = new JEditorPane();
        myPane.setContentType("text / plain");
        myPane.setText("Sleeping is necessary for a healthy body. "
                + " But sleeping in unnecessary times may spoil our health, wealth and studies."
                + " Doctors advise that the sleeping at improper timings may lead for obesity during the "
                + "students days.");

        myFrame.setContentPane(myPane);
        myFrame.setVisible(true);
    }

}
