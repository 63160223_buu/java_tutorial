/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;

/**
 *
 * @author focus
 */
public class JavaJRadioButton extends JFrame implements ActionListener {

    JRadioButton rb1, rb2;
    JButton b;

    JavaJRadioButton() {
        rb1 = new JRadioButton("Male");
        rb1.setBounds(100, 50, 100, 30);
        rb2 = new JRadioButton("Female");
        rb2.setBounds(100, 100, 100, 30);
        
        ButtonGroup bg = new ButtonGroup(); //ButtonGroup คือ ทำให้สามารถเลือกรายการที่กำหนดได้เพียงแค่ 1 รายการ
        bg.add(rb1); bg.add(rb2);//คือ add rb1 , rb2 ลงใน ButtonGroup
        
        b = new JButton("Click");
        b.setBounds(100, 150, 80, 30);
        b.addActionListener(this); // กล่าวคือ add ปุ่มลงใน ActionListener โดยใช้คำว่า this แทน
       
        add(rb1); add(rb2); add(b); // Add สิ่งต่างๆลงใน JFrame
        
        setSize(300, 300); //กำหนดขนาด JFrame
        setLayout(null);//กำหนดให้เมื่อใครเข้ามาใช้ ให้คนนั้นเป็นคนจัดองค์ประกอบเอง
        setVisible(true);//ทำให้มองเห็น JFrame
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(rb1.isSelected()){//ถ้า rb1 ถูกเลือก
        JOptionPane.showMessageDialog(this, "You are Male.");
        rb1.setBackground(Color.blue);
        
    }
        if(rb2.isSelected()){//ถ้า rb2 ถูกเลือก
        JOptionPane.showMessageDialog(this, "You are Female.");
        rb2.setBackground(Color.pink);
        }
    }
    
    public static void main(String[] args) {
        new JavaJRadioButton();
    }
}
