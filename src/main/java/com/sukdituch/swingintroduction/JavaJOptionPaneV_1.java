/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author focus
 */
public class JavaJOptionPaneV_1 {
    
    JFrame f;
    
    JavaJOptionPaneV_1(){
        f = new JFrame();
        
        JOptionPane.showMessageDialog(f, "Hello, Wellcome to Javapoint."); // (component, กับแสดง Dialog Message)
        
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    public static void main(String[] args) {
        new JavaJOptionPaneV_1();
    }
}
