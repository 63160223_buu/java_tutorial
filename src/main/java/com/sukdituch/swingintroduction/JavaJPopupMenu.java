/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

/**
 *
 * @author focus
 */
public class JavaJPopupMenu {

    JavaJPopupMenu() {//{} is Anonymous Class

        final JFrame f = new JFrame("Popup Menu");
        final JLabel lbl = new JLabel();
        lbl.setHorizontalAlignment(JLabel.CENTER);//กำหนดตำแหน่งให้อยู่ตรงกลาง
        lbl.setSize(400, 100);//กว้าง สูง
        lbl.setBackground(Color.pink);
        lbl.setOpaque(true);
        
        final JPopupMenu popupmenu = new JPopupMenu("Edit");//สร้าง popup Menu ชื่อว่า Edit
        JMenuItem cut = new JMenuItem("Cut"); // สร้าง Menu Item
        JMenuItem copy = new JMenuItem("Copy");
        JMenuItem paste = new JMenuItem("Paste");
        
        popupmenu.add(cut);
        popupmenu.add(copy);
        popupmenu.add(paste);
        
        f.addMouseListener(new MouseAdapter() {// ทำงานเมื่อเมาส์ไปสัมผัส
            public void mouseClicked(MouseEvent e){
                
                popupmenu.show(f, e.getX(), e.getY());//(component, int x, int y)
                
            }
        });

        cut.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                lbl.setText("- Cut - MenuItem Clicked.");
            }
                });
        
        copy.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                lbl.setText("- Copy - MenuItem Clicked.");
            }
                });
        
        paste.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                lbl.setText("- Paste - MenuItem Clicked.");
            }
                });
        
        f.add(lbl);
        f.add(popupmenu);
        f.setSize(400, 400);
        f.setLayout(null);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
    }
    public static void main(String[] args) {
        new JavaJPopupMenu();
    }
    
}
