/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import java.awt.FlowLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JFrame;
import javax.swing.JToggleButton;

/**
 *
 * @author focus
 */
public class JavaJToggleButton extends JFrame implements ItemListener {

    public static void main(String[] args) {
        new JavaJToggleButton();
    }

    private JToggleButton button;

    JavaJToggleButton() {
        setTitle("Wellcome to JToggleButton");
        setLayout(new FlowLayout());//FlowLayout เป็นรูปแบบของการจัดวาง layout โดยจะเรียง component จากซ้ายไปขวา
        setJToggleButton(); //เซตค่าของปุ่ม
        setAction();//เซตค่าเมื่อมีการกระทำกับปุ่ม
        setSize(200, 200);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    private void setJToggleButton() {
        button = new JToggleButton("LOGIN");
        add(button);//เพิ่ม button ลงใน JtoggleButton
    }

    private void setAction() {
        button.addItemListener(this); //เพิ่มaddItemListener ลงในปุ่ม
    }


    @Override
    public void itemStateChanged(ItemEvent e) {
        if(button.isSelected())//ถ้าปุ่มถูกกด
            button.setText("LOGOUT");
        else
            button.setText("LOGIN");
    }
}
