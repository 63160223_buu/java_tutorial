/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextArea;

/**
 *
 * @author focus
 */
public class JavaJMenuItemAndJMenu implements ActionListener{

    JFrame f;
    JMenuBar mb;
    JMenu file,edit,help; // แถบเลือกด้านบน
    JMenuItem cut, copy, paste, selectAll; //เลือกต่อจากเลือกแถบด้านบน
    
    JTextArea txta;
    
    JavaJMenuItemAndJMenu(){ // {} Anonymous Class
        
        f = new JFrame();
        cut = new JMenuItem("Cut");
        copy = new JMenuItem("Copy");
        paste = new JMenuItem("Paste");
        selectAll = new JMenuItem("Select All");
        
        cut.addActionListener(this);
        copy.addActionListener(this);
        paste.addActionListener(this);
        selectAll.addActionListener(this);
        
        mb = new JMenuBar();
        file = new JMenu("File");
        edit = new JMenu("Edit");
        help = new JMenu("Help");
        
        
        // add MenuItem เข้าไปใน edit
        edit.add(cut);  
        edit.add(copy);
        edit.add(paste);
        edit.add(selectAll);
        
        mb.add(file); 
        mb.add(edit);
        mb.add(help);
        
        txta =new JTextArea();
        txta.setBounds(5, 5, 360, 320);
        
        f.add(mb);
        f.add(txta);
        f.setJMenuBar(mb); // ใส่mb เข้าไปใน MenuBar ในเฟรม
        f.setLayout(null);//กำหนดให้ใครก็ตามที่เข้ามาใช้โปรแกรมเป็นคนจัดองค์ประกอบต่างๆเอง
        f.setSize(400, 400);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
          
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {//actionPerformed คือจะทำงานเมื่อ มีการกระทำใดๆเกิดกับ component
        if(e.getSource() == cut) //ถ้าเลือก == cut ให้ใช้ cut
            txta.cut();
        if(e.getSource() == copy)
            txta.copy();
        if(e.getSource() == paste)
            txta.paste();
        if(e.getSource() == selectAll)
            txta.selectAll();
    }
    public static void main(String[] args) {
        new JavaJMenuItemAndJMenu();
    }
}
