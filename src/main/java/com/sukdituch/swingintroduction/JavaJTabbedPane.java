/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;

/**
 *
 * @author focus
 */
public class JavaJTabbedPane {
    
    JFrame f;
    
    JavaJTabbedPane(){
        
        f= new JFrame("JTabbedPane Example");
        JTextArea ta = new JTextArea(200, 200);//เป็นกล่องใส่ข้อความ
        
        JPanel p1 = new JPanel();//JPanel จัดอยู่ในกลุ่มของ Container ไว้สำหรับจัดกลุ่มของ Component Controls ให้อยู่ในกลุ่มเดียวกัน
        p1.add(ta);
        JPanel p2 = new JPanel();
        JPanel p3 = new JPanel();
        
        JTabbedPane tp = new JTabbedPane();
        tp.setBounds(50, 50, 200, 200);
        tp.add("Main", p1);
        tp.add("Visit", p2);
        tp.add("Help", p3);
        
        f.add(tp);
        f.setSize(400, 400);
        f.setLayout(null);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    public static void main(String[] args) {
        new JavaJTabbedPane();
    }
    
}
