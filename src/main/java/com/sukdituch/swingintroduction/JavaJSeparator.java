/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 *
 * @author focus
 */
public class JavaJSeparator {

    JMenu menu, submenu;
    JMenuItem i1, i2, i3, i4, i5;

    JavaJSeparator() {

        JFrame f = new JFrame("Separator ExampleV_1");
        JMenuBar mb = new JMenuBar();
        menu = new JMenu("Menu");
        i1 = new JMenuItem("Oop");
        i2 = new JMenuItem("Data Base");
        i3 = new JMenuItem("Prob Static");
        i4 = new JMenuItem("Calculus");
        i5 = new JMenuItem("Eng for CS");

        menu.add(i1);
        menu.addSeparator();//เป็นเสร้างเส้นขอบกั้นแบ่งแยกระหว่างกัน
        menu.add(i2);
        menu.addSeparator();//เป็นเสร้างเส้นขอบกั้นแบ่งแยกระหว่างกัน
        menu.add(i3);
        menu.addSeparator();//เป็นเสร้างเส้นขอบกั้นแบ่งแยกระหว่างกัน
        menu.add(i4);
        menu.addSeparator();//เป็นเสร้างเส้นขอบกั้นแบ่งแยกระหว่างกัน
        menu.add(i5);

        mb.add(menu);// เพิ่ม menu ลงใน mb
        
        f.setJMenuBar(mb);//กำหนดให้MenuBar ของ frame มีค่าเป็น mb 
        f.setSize(400, 400);
        f.setLayout(null);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    public static void main(String[] args) {
        new JavaJSeparator();
    }
}
