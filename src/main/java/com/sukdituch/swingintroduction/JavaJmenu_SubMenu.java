/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 *
 * @author focus
 */
public class JavaJmenu_SubMenu {

    JMenu menu, submenu;
    JMenuItem i1, i2, i3, i4, i5;
    
    JavaJmenu_SubMenu(){
        
         JFrame f= new JFrame("Menu and MenuItem Example");  
          JMenuBar mb=new JMenuBar();  
          menu=new JMenu("Menu");  
          submenu=new JMenu("Other");  //สร้างเมนูย่อย
          i1=new JMenuItem("cut");  
          i2=new JMenuItem("copy");  
          i3=new JMenuItem("paste");  
          i4=new JMenuItem("Select All");  
          i5=new JMenuItem("Delete");  
          menu.add(i1); menu.add(i2); menu.add(i3);  //add i1, i2, i3 เข้าไปในเมนู
          submenu.add(i4); submenu.add(i5);  //add i4, i5 เข้าไปใน sub
          menu.add(submenu);  //เพิ่ม sub เข้าไปในเมนูหลัก
          mb.add(menu);  //เพิ่ม เมนู เข้าไปใน MenuBar
          f.setJMenuBar(mb);  //กำหนดให้ MenuBar ใน fram == mb
          f.setSize(400,400);  
          f.setLayout(null);  
          f.setVisible(true);
          f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
    }
    
    public static void main(String[] args) {
        new JavaJmenu_SubMenu();
    }
    
}
