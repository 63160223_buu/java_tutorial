/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JComponent;
import javax.swing.JFrame;

/**
 *
 * @author focus
 */
class MyJComponent extends JComponent { //สร้าง classแยก

    public void paint(Graphics g) {//paint (Graphics) คือวาด graphic
        g.setColor(Color.pink);
        g.fillRect(30, 30, 100, 100);//กำหนดตำแหน่ง และขนาด
    }
}

public class JavaJComponent {//JComponent เป็นแม่แบบทุกตัวใน Swing ที่ขึ้นต้นด้วย J
    public static void main(String[] args) {
        MyJComponent comp = new MyJComponent();
        
        //สร้าง JFrame
        JFrame.setDefaultLookAndFeelDecorated(true);
        JFrame frame = new JFrame("JComponent Example");
        frame.setSize(300, 200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
         // add the JComponent to main frame 
         frame.add(comp);
         frame.setVisible(true);
         
    }
}
