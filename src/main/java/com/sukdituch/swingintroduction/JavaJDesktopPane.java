/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import java.awt.BorderLayout;
import java.awt.Container;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;

/**
 *
 * @author focus
 */
public class JavaJDesktopPane extends JFrame {//JDesktopPane คือ สร้างหน้าต่างเพิ่มขึนมา

    public JavaJDesktopPane() {//{} Anonymous class คือสร้าง class ขึ้นมาตรงไหนก็ได้

        CustomDesktopPane desktopPane = new CustomDesktopPane();
        Container contentPane = getContentPane();
        contentPane.add(desktopPane, BorderLayout.CENTER);//BorderLayout เป็นรูปแบบการจัดส่ง Layout ที่กำหนดตำแหน่งแบบแน่นอน
        desktopPane.display(desktopPane);// display คือ เรากำหนดชื่อ Method เพื่อที่เราจะไปทำ Method ต่อ

        setTitle("JDesktopPane Example");
        setSize(300, 350);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        new JavaJDesktopPane();
    }

    class CustomDesktopPane extends JDesktopPane {

        int numFrames = 3, x = 30, y = 30; //สร้างจำนวนเต็มกับค่า

        private void display(CustomDesktopPane dp) {
            for (int i = 0; i < numFrames; i++) {

                JInternalFrame jframe = new JInternalFrame("InternalFrame " + i, true, true, true, true);

                jframe.setBounds(x, y, 250, 85);
                Container c1 = jframe.getContentPane(); //สร้างตัวแปร c1 ไว้เก็บข้อมูล
                c1.add(new JLabel("I love my country"));//JLabel จะไม่สามารถแก้ไขตัวอักษรนั้นได้
                dp.add(jframe);
                jframe.setVisible(true);
                y += 85; // ตำแหน่งที่ y เมื่อวนลูป1ครั้งจะ + เพิ่มอีก 85 //เพื่อให้โปรแกรมไม่ทับซ้อนกัน

            }
        }

    }
}
