/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.Authenticator;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author focus
 */
public class JavaJLabel extends Frame implements ActionListener{// คลาสสืบทอด Frame และ implements ActionListener
    JTextField tf; JLabel l; JButton b; // กำหนดตัวแปรไว้นอก fn
    
    JavaJLabel(){ 
        
        tf = new JTextField();
        tf.setBounds(50, 50, 250, 20); //กำหนดตำแหน่ง x,y,กว้าง,สูง
        l = new JLabel(); // JLabel คือ ข้อความที่ไม่สามารถแก้ไข หรือแตะต้องได้
        l.setBounds(50, 100, 250, 20); //กำหนดตำแหน่ง x,y,กว้าง,สูง
        b = new JButton("Find IP"); // สร้างปุ่มที่มีชื่อว่า Find IP
        b.setBounds(50, 150, 95, 30); //กำหนดตำแหน่ง x,y,กว้าง,สูง
        b.addActionListener(this);// คือ กำหนดให้ปุ่มมีการตอบสนองเมื่อทำการกด
        add(b); add(tf); add(l);
        setSize(400, 400); // กำหนดขนาดโปรแกรม
        setLayout(null); // กำหนดผู้จัดการโปรแกรม เป็นคนจัดเรียงเอง
        setVisible(true); // กำหนดให้มองเห็นโปรแกรม
        
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {  //ทำงานเมื่อกดปุ่ม
        try{  //ลองอีกครั้ง
        String host=tf.getText();  //สร้างString ที่มีชื่อว่า host ที่มีค่าเป็น JText Field ชื่อว่า tf 
        String ip = java.net.InetAddress.getByName(host).getHostAddress();   // สร้าง String IP ที่เก็บรวบรวมJava IP ของเว็ปต่างๆ Youtube, facebook ก็หาได้
        l.setText("IP of "+host+" is: "+ip);
        }catch(Exception ex){System.out.println(ex);}  //ดักจับ Error Exception และทำการปริ้นตัวที่ Error
        
        
    }  
    public static void main(String[] args) {
        JavaJLabel jjlb = new JavaJLabel(); // สร้าง object
        
    }
}
