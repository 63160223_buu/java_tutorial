/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author focus
 */
public class JavaJDialog {//JDialog คือ หน้าต่างที่เด้งต่อจาก frame
    //static method เป็น method ที่มีลักษณะคล้าย static variable คือ การอ้างถึงโดยไม่จำเป็นต้องประกาศ Object 
    //หรืออาจจะประกาศ แต่ยังไงก็เป็นตัวเดียวกันอยู่ดี แต่การอ้างถึงระหว่าง method อื่นๆนั้น หากเป็น  non-static method 
    //หรือ non-static variable ก็จะไม่สามารถเรียกจาก static method ได้ ในทางกลับกัน non-static method จะ
    //สามารถเรียก static variable & static method ได้ตามปกติ

    private static JDialog d;

    JavaJDialog(){
        
        JFrame f = new JFrame();
        
        d = new JDialog(f, "Dialog Example", true);
        d.setLayout(new FlowLayout());//FlowLayout เป็นรูปแบบของการจัดวาง layout โดยจะเรียง component จากซ้ายไปขวา และถ้าหากหมดบรรทัดก็จะขึ้นต้นบรรทัดใหม่ เหมือนการเขียนหนังสือ
        
        JButton b = new JButton("OK");
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
                JavaJDialog.d.setVisible(false); //กำหนดให้Class JavaJDialog แสดง d ให้ปิดการมองเห็น
                
            }
        });
        
        d.add(new JLabel("Click Button to Continue."));
        d.add(b);
        d.setSize(300, 300);
        d.setVisible(true);
        d.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
    }
    public static void main(String[] args) {
        
        new JavaJDialog();
        
    }
}
