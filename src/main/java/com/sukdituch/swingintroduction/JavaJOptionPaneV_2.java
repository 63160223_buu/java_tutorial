/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author focus
 */
public class JavaJOptionPaneV_2 {
    
    JFrame f;

    JavaJOptionPaneV_2(){ // {} Anonymous Class
        f = new JFrame();
        
        //(Component parentComponent, Object message,  String title, int messageType)
        //(Component , แสดงข้อความ, หัวข้อการแจ้งเตือน, ชนิดของ message ที่แจ้ง)
        JOptionPane.showMessageDialog(f, "Failed To Update.", "Alert", JOptionPane.WARNING_MESSAGE);
    }
            
    public static void main(String[] args) {
        new JavaJOptionPaneV_2();
    }
    
}
