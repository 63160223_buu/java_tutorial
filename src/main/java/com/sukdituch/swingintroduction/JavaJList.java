/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;

/**
 *
 * @author focus
 */
public class JavaJList {

    JavaJList() {

        JFrame f = new JFrame("Select Languages");

        final JLabel label = new JLabel(); // final คือ ไม่สามารถเปลี่ยนแปลงได้
        label.setSize(500, 100);

        JButton b = new JButton("Show");
        b.setBounds(200, 150, 80, 30);

        final DefaultListModel<String> l1 = new DefaultListModel<>(); // DefaultListModel<>คือ สร้างList โดยกำหนดให้ภายใน List เป็นข้อมูลชนิดใด <String> เป็นต้น
        l1.addElement("C");//addElement() ใช้เพิ่ม ข้อมูล เข้าไปใน vector ในตำแหน่งสุดท้าย
        l1.addElement("C++");
        l1.addElement("PYTHON");
        l1.addElement("JAVA");
        l1.addElement("PHP");

        final JList<String> list1 = new JList<>(l1);//ให้ JList เป็น String โดยใช้ข้อมูลใน l1
        list1.setBounds(100, 100, 75, 75);//กำหนดตำแหน่งx,y ขนาดกว้าง, สูง

        DefaultListModel<String> l2 = new DefaultListModel<>();//DefaultListModel<>คือ สร้างList แล้วเก็บข้อมูลต่างๆไว้ใน List
        l2.addElement("Turbo C++");//addElement() ใช้เพิ่ม ข้อมูล เข้าไปใน vector ในตำแหน่งสุดท้าย
        l2.addElement("Strsts");
        l2.addElement("Spring");
        l2.addElement("YII");

        final JList<String> list2 = new JList<>(l2);//กำหนดให้ JList เป็น String โดยใช้ข้อมูลในภายใน l2 ที่กำหนดไว้
        list2.setBounds(100, 200, 75, 75);

        // add Object ต่างๆเข้าไปใน frame
        f.add(list1);
        f.add(list2);
        f.add(b);
        f.add(label);

        f.setSize(450, 450);
        f.setLayout(null); // กำหนดให้ใครก็ตามที่เข้ามาใช้โปรแกรม เป็นคน setting ค่าต่างๆเอง
        f.setVisible(true);//ทำให้มองเห็น frame
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//เมื่อปิดโปรแกรม Java จะหยุดรัน

        b.addActionListener(new ActionListener() { //สร้าง Action Listener ขึ้นมาใหม่ และ add เข้าไปใน JButton b

            @Override
            public void actionPerformed(ActionEvent e) {
                String data = ""; //กำหนดให้ ยังไม่มีข้อมูลใดๆใน data

                //getSelectedIndex ( ) จะคืนค่าตำแหน่งของข้อมูลที่ถูกเลือกโดยคืนค่ามาเป็นตัวแปรประเภทที่กำหนด
                if (list1.getSelectedIndex() != -1) {// ถ้าเลือกข้อมูลใน list1 ที่ไม่เท่ากับ -1
                    data = "Programming language Selected: " 
                            + list1.getSelectedValue(); // getSelectedValue() ใช้คืนค่า ข้อควาาม ของรายการที่ถูกเลือก ใน object JList
                    label.setText(data);//ให้ lbl ปริ้นค่าที่กำหนดไว้ใน data
                }
                if(list2.getSelectedIndex() != -1){// ถ้าเลือกข้อมูลใน list2 ที่ ไม่เท่ากับ -1
                    
                    data += "FrameWord Selected: "; //data = data + "FrameWord Selected: "
                    
                    //ลูปกำหนดให้ frame เป็น Object เป็น list2 ที่เราเลือกในโปรแกรม
                    for(Object frame :list2.getSelectedValues()){
                    data += frame + " "; // data += คือ เพิ่มอันใหม่ใส่ต่อจากอันเก่า
                }
            }
            label.setText(data);// ให้ label แสดงค่าภายใน data
            }
        });    
    }
    public static void main(String[] args) {
        new JavaJList();
    }
}
