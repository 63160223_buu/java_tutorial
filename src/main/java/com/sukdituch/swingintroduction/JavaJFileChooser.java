/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextArea;

/**
 *
 * @author focus
 */
public class JavaJFileChooser extends JFrame implements ActionListener{

    JMenuBar mb; //JMenuBar คือแถบเครื่องมือด้านบน
    JMenu file;
    JMenuItem open;
    JTextArea ta;
    
    JavaJFileChooser(){
        
        open = new JMenuItem("Open File"); //JMenuItem ต่อจากเลือก JMenu
        open.addActionListener(this);
        
        file = new JMenu("File");
        file.add(open);
        
        mb = new JMenuBar();//JMenuBar คือแถบเครื่องมือด้านบน
        mb.setBounds(0, 0, 800, 20);//ตำแหน่งx, y, กว้าง, สูง
        mb.add(file);
        
        ta = new JTextArea(800, 800);//กล่องข้อมูล
        ta.setBounds(0, 20, 800, 800);
        
        add(mb);
        add(ta);
        
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        
        if(e.getSource() == open){
            JFileChooser fc = new JFileChooser(); //เลือกไฟล์
            
            int i = fc.showOpenDialog(this); //เปิดหน้าต่าง i ขึ้นมา
            
            if(i==JFileChooser.APPROVE_OPTION){//APPROVE_OPTION คือการอนุมัติให้สามารถใช้ Optionนั้นได้
                File f = fc.getSelectedFile();// 
                String filepath = f.getPath();//getPath คือ คืนค่าตามที่เรากำหนด ในที่นี้คือ String
                
                try{
                    //BufferReader เป็นคลาสที่ช่วยในการอ่านข้อความจากอินพุตสตรีมอักขระ มันอ่านตัวละครโดยใช้ Reader อื่น
                    BufferedReader br=new BufferedReader(new FileReader(filepath)); //FileReader คือ การอ่านไฟล์ ผลตอบแทนหลังจากอ่านไฟล์เสร็จ = -1
                    String s1="",s2="";
                    //readLine คือ อ่านค่า
                    while((s1 = br.readLine())!=null){//while คือ ถ้าเงื่อนไขเป็นจริงโปรแกรมจะทำงาน
                    s2 += s1 +"\n";
                    }
                    ta.setText(s2);
                    br.close();//close() ใช้ในการยกเลิกการติดต่อฐานข้อมูลของ ResultSet
                    
                    //.printStackTrace(); เป็นเมธอดที่ใช้สำหรับ IDE ในการบอกรายละเอียดเกี่ยวกับข้อผิดพลาดให้กับเรา เช่น 
                    //บรรทัดที่ทำให้เกิดความผิดพลาด และคลาสที่รัน ใช้สำหรับการ debug โปรแกรม
                }catch (Exception ex) {ex.printStackTrace();}
            }
        }  
    }
    public static void main(String[] args) {
        JavaJFileChooser fc = new JavaJFileChooser();
        fc.setSize(500, 500);
        fc.setLayout(null);
        fc.setVisible(true);
        fc.setDefaultCloseOperation(EXIT_ON_CLOSE);
        
    }
}
