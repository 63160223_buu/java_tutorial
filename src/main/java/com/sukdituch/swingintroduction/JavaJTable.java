/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author focus
 */
public class JavaJTable {
    
    public static void main(String[] args) {
        
        JFrame f = new JFrame("Table Example");
        
        String data[][] = {{"101","Suphisara","670000"}, // [][] คือ Array 2 มิติ
            {"102","Thaweesab","780000"}, {"103","Anantavit","70000"}};
            
        String column[] = {"ID","Name","SALARY"}; //หัวข้อต่างๆ ของแต่ละ column
        
        final JTable jt = new JTable(data, column); // สร้าง JTable (data (คือแถว), column(คือ column))
        jt.setCellSelectionEnabled(true);// ถ้ากำหนด true จะทำให้เป็นการกำหนดว่าให้เลือกข้อมูลในรูปแบบ cell (รูปแบบ ช่องตาราง)
        
        //SelectionModel เป็นการคืนค่า Object ของjt พื่อใช้กำหนด mode ให้กับ jt ว่าสามารถเลือกข้อมูลได้เพียงข้อมูลเดียวหรือหลายข้อมูล
        ListSelectionModel select = jt.getSelectionModel();//ListSelectionModel มักใช้ร่วมกับ JScroll
        select.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);//กำหนดให้สามารถเลือกได้เพียงแค่ 1 อย่าง
        select.addListSelectionListener(new ListSelectionListener() {  // {} คือ Anonymous class
            @Override
            public void valueChanged(ListSelectionEvent e) {
                String Data = null; //null คือตัวแปรที่ว่างๆ ไม่มีค่าอะไร
                
                int[] row = jt.getSelectedRows(); // row ในที่นี้คือ String Data ที่กำหนดไว้
                int[] columns = jt.getSelectedColumns();// คือ String column ที่กำหนดไว้ด้านบน
                
                // วนลูป 2 ชั้น
                for (int i = 0 ; i < row.length; i++){
                    for (int j = 0; j < columns.length; j++){
                        Data = (String) jt.getValueAt(row[i], columns[j]); //วนลูป Data โดยแปลงให้เป็น String
                    }
                }
                
                System.out.println("Table element selected is : " + Data); // เมื่อวนลูปเสร็จแล้วให้แสดงข้อมูลภายในวงเล็ป
                
            }
            
        });
        // JScrollPane จัดอยู่ในกลุ่มของ Container มีหน้าที่ไว้สำหรับสร้าง Scroll ให้กับ Component Controls ในกรณีที่ข้อมูลเกินขอบเขตที่สร้างไว้
        JScrollPane sp = new JScrollPane(jt); 
        
        f.add(sp); //add sp ลงใน frame
        f.setSize(300, 200);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
}
