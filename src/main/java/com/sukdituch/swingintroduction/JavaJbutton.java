/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author focus
 */
public class JavaJbutton {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Button Example");// สร้าง JFrame ที่มีชื่อframe ว่า"Button Example"
        JButton button = new JButton("Click Here");// สร้าง Button ที่มีชื่อว่า Click Here
        
        button.setBounds(50, 100, 95, 30);// กำหนดตำแน่งของ button ตำแหน่งx,y ,ความกว้าง,สูง
        frame.add(button);// เพิ่ม button เข้าไปใน frame
        frame.setSize(400, 400);//กำหนดขนาดframe = กว้าง400 สูง400
        frame.setLayout(null);//กำหนดให้ เมื่อใครเข้ามาใช้เป็นคนปรับแต่งเอง
        frame.setVisible(true);//ทำให้แสดง frame 
        
        
    }
}
