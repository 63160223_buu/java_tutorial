/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swingintroduction;

import javax.swing.JFrame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JOptionPane;

/**
 *
 * @author focus
 */
public class JavaJOptionPaneV_4 extends WindowAdapter {
    //WindowAdapter เป็น Interface ในการ ดักฟังเหตุการณ์ ของการทำงานของ window
    //โดยมี abstract method คือ 
    //windowActivated คือจะทำงานเมื่อ window ได้รับ focus
    //windowDeactivated คือจะทำงานเมื่อ window สูญเสีย focus
    //windowOpened คือจะทำงานเมื่อ window ถูกเปิดเป็นครั้งแรก ( ถูก load )
    //windowClosing คือจะทำงานเมื่อ window กำลังถูกปิด
    //windowClosed คือจะทำงานเมื่อ window ถูกปิดการใช้งาน
    //windowIconified คือจะทำงานเมื่อ window ถูก click ที่ปุ่ม minimize
    //windowDeiconified คือจะทำงานเมื่อ window ถูก click ที่ปุ่ม maximize
    
    JFrame f;

    JavaJOptionPaneV_4() {

        f = new JFrame();
        f.addWindowListener(this);
        f.setSize(300, 300);
        f.setLayout(null); //กำหนดให้ใครก็ตามที่เข้ามาใช้โปรแกรม เป็นคนจัดองค์ประกอบต่างๆเอง
        //DO_NOTHING_ON_CLOSE คือ ถึงจะกดปิด JFrame แต่ยังไม่ปิด JFrame
        f.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        f.setVisible(true);
        
    }
    
    public void windowClosing(WindowEvent e){
        
        int a = JOptionPane.showConfirmDialog(f, "Are you sure?");//(component, แสดงข้อความ)
        
        if(a == JOptionPane.YES_OPTION) // YES_OPTION คือ fn หน้าต่าง แสดง [Yes] [No] [Cancle]
            f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            
    }

    public static void main(String[] args) {
        new JavaJOptionPaneV_4();
    }
}
